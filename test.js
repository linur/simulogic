var NUMBER_OF_INPUT_WIRES = 5
var NUMBER_OF_OUTPUT_WIRES = 3

function createCheckbox(id, text, idParent) {
    var checkbox = document.createElement("input")
    checkbox.setAttribute("type", "checkbox")
    checkbox.setAttribute("id", id);
    document.getElementById(idParent).appendChild(checkbox)

    var checkboxText = document.createTextNode(text)
    document.getElementById(idParent).appendChild(checkboxText)
}

function createCheckboxesWires(choice) {
    var numberOfWires
    var id = "id"
    var idParent = "id"

    switch (choice) {
        case "input":
            numberOfWires = NUMBER_OF_INPUT_WIRES
            id += "Input"
            idParent += "Body"
            break;
        case "output":
            numberOfWires = NUMBER_OF_OUTPUT_WIRES
            id += "Output"
            idParent += "FormOutputWires"
            break;
        default:
            break;
    }
    for (let i = 1; i < numberOfWires+1; i++) {
        createCheckbox(id+"Wire"+i, choice+"Wire"+i, idParent)
    }
}

var selectedInputWires = []

function showChronographsSelectedInputWires() {
    for (let i = 1; i < NUMBER_OF_INPUT_WIRES+1; i++){
        if(document.getElementById("idInputWire"+i).checked){
            selectedInputWires.push(i)
        }
    }
    document.getElementById("selectedWires").innerText = "You selected wires "+selectedInputWires.toString()
    
    selectedInputWires.forEach(wire => {
        createTextField("idChronograph"+wire,"chronograph wire "+wire)
    });

    var submitChronographsButton = document.createElement("input")
    submitChronographsButton.setAttribute("type", "submit")
    submitChronographsButton.setAttribute("value","Send")
    document.getElementById("formChronographs").appendChild(submitChronographsButton)

}

function createTextField(id, text) {
    var chronographText = document.createTextNode(text+": ")
    document.getElementById("formChronographs").appendChild(chronographText)

    var textField = document.createElement("input")
    textField.setAttribute("type", "text")
    textField.setAttribute("id", id);
    document.getElementById("formChronographs").appendChild(textField)

    var newLine = document.createElement("br")
    document.getElementById("formChronographs").appendChild(newLine)
}

var inputChronographs = []

function sendChronographs() {
    selectedInputWires.forEach(wire => {
        var chronograph = document.getElementById("idChronograph"+wire).value
        inputChronographs.push(chronograph)
    });

    var text = document.createElement("p")
    text.innerText = "Now, choose the output wires to observe"
    document.getElementById("idFormOutputWires").appendChild(text)
    createCheckboxesWires("output")   

    var submitButton = document.createElement("input")
    submitButton.setAttribute("type","submit")
    submitButton.setAttribute("value","Simulation")
    document.getElementById("idFormOutputWires").appendChild(submitButton)
}

var selectedOutputWires = []
var outputWiresChronograms = []

function sendSelectedOutputWires() {
    for (let i = 1; i < NUMBER_OF_OUTPUT_WIRES+1; i++) {
        if (document.getElementById("idOutputWire"+i).checked) {
            selectedOutputWires.push(i)
        }
    }

    showChronographsSelectedOutputWires()
}

function showChronographsSelectedOutputWires() {
    selectedOutputWires.forEach(wire => {
        chronograph = generateRandomChronograph(10)
        var text = document.createTextNode("chronograph output wire "+wire+": "+chronograph)
        document.body.appendChild(text)

        var newLine = document.createElement("br")
        document.body.appendChild(newLine)
    });
}

function generateRandomChronograph(length) {
    var randomChronograph = []
    for (let i = 0; i < length; i++) {
        randomChronograph.push(Math.round(Math.random()))
    }
    return randomChronograph
}